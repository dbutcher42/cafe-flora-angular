import { HttpTestingController } from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { Router } from '@angular/router';

import { CustomerHomePageComponent } from './customer-home-page.component';

describe('CustomerHomePageComponent', () => {
  let component: CustomerHomePageComponent;
  let fixture: ComponentFixture<CustomerHomePageComponent>;
  let router: Router;
  let mockClient: { get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy };
  let httpMock: HttpTestingController;
  let location: Location;

  // beforeEach(async () => {
  //   await TestBed.configureTestingModule({
  //     declarations: [ CustomerHomePageComponent ]
  //   })
  //   .compileComponents();
  // });


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
      ],
      declarations: [CustomerHomePageComponent],
      providers: [
        { provide: HttpClient, useValue: mockClient }
      ]
    })
    .compileComponents();
    fixture = TestBed.createComponent(CustomerHomePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.inject(Router);
    mockClient = TestBed.get(HttpClient);
  });
  /*
  TODO need to test:  Menu, Cart, Log out
  */
  it("should go to home's page works", fakeAsync(() => {
    let promise = new Promise(resolve => {
      setTimeout(resolve, 10);
    });
    let done = false;
    promise.then(() => (done = true));
    tick(50);
    expect(done).toBeTruthy();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
