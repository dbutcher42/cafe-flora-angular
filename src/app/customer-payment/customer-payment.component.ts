import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../services/customer';
import { CustomerService } from '../services/customer.service';
import { MockBank } from '../services/mockbank';
import { MockbankService } from '../services/mockbank.service';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-customer-payment',
  templateUrl: './customer-payment.component.html',
  styleUrls: ['./customer-payment.component.css']
})
export class CustomerPaymentComponent implements OnInit {

  constructor(private router:Router, 
    private custServ:CustomerService,
    private mbServ:MockbankService, 
    private ordServ:OrderService) { }

  customer:Customer;
  order:Order;
  total:number;
  creditcardnumber:String;
  creditcardname:String;
  creditcardmonth:number;
  creditcardyear:number;
  creditcardcode:number;
  mockbank:MockBank;
  message:String;
  orderId:number;
  customerId:number;

  //we get the initial order/customer data from session datat, go to login if session data not found
  ngOnInit(): void {
    this.customer = JSON.parse(localStorage.getItem("customerInfo"));
    this.order = JSON.parse(localStorage.getItem("customerOrder"));
    if (this.customer == null) {
      this.router.navigate(['/login']);
    }
    this.customerId = this.customer.customerId;
    this.total = this.order.total;
  }

  //redirect back to checkout
  backtocheckout() {
    this.router.navigate(['/checkout']);
  }

  //if they check the checkbox use the stored cc data, if they uncheck it clear the cc fields
  eventCheck(event) {
    if ( event.target.checked ) {
      this.creditcardname = this.customer.creditCardName;
      this.creditcardnumber = this.customer.creditCardNumber;
      this.creditcardmonth = this.customer.creditCardMonth;
      this.creditcardyear = this.customer.creditCardYear;
      this.creditcardcode = this.customer.creditCardCode;
    } else {
      this.creditcardname = null;
      this.creditcardnumber = null;
      this.creditcardmonth = null;
      this.creditcardyear = null;
      this.creditcardcode = null;
    }
  }

  //compare their cc info to that in the mock bank, reject if not found or insufficient funds, otherwise go to verification screen
  processpayment() {
    this.message = "Processing Payment";
    this.custServ.verifyCC(this.creditcardnumber, this.creditcardname, this.creditcardcode, this.creditcardmonth, this.creditcardyear, this.total).subscribe(
      response => {
        let verify:String = response;
        if (verify == "accepted") {
          this.message = "Payment Accepted"
          this.router.navigate(['/verified']);
          this.ordServ.completeOrder(this.order.orderId).subscribe(
            response => {
              this.message = "Order Submited";
            }
          )
        } else {
          if (verify == "insufficent") {
            this.message = "Insufficient Funds";
          } else {
            this.message = "Credit Card Rejected"  
          }
        }
      }
    );
  }
}
