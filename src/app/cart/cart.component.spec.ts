import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { By } from '@angular/platform-browser';

import { CartComponent } from '../cart/cart.component';
import { CustomerNavBarComponent } from '../customer-nav-bar/customer-nav-bar.component';
import { Coupon } from '../services/coupon';
import { CouponStatus } from '../services/couponstatus';
import { Customer } from '../services/customer';
import { Distribution } from '../services/distribution';
import { MenuItem } from '../services/menuitem';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';
import { OrderItem } from '../services/orderitem';
import { OrderitemService } from '../services/orderitem.service';
import { OrderStatus } from '../services/orderstatus';
import { ifStmt } from '@angular/compiler/src/output/output_ast';




describe('CartComponent', () => {
  class MockService {
    getOrderItemsByOrderId() {return mockClient.get()}
    addOrderItem() {return mockClient.put()}
    removeOrderItem() {return mockClient.delete()}
    removeAllOrderItems() {return mockClient.delete()}
    updateOrderTotal() {return mockClient.put()}
  }


  let dummyCustomerData2:Customer = {
    customerId : 1,
    email : "email@email.com",
    password : "password",
    firstName : "John",
    lastName : "Smith",
    address : "123 Main St",
    city : "New City",
    state : "NY",
    zipCode : "01102",
    phone : "616 314 2589",
    creditCardNumber : "1111111111111111",
    creditCardName : "John Smith",
    creditCardMonth : 1,
    creditCardYear : 2021,
    creditCardCode : 999
    }
  
    let dummyOrderStatusData2:OrderStatus = {
      statusId:1,
      statusName:"Building"
    }
    
    let dummyDistributionData2:Distribution = {
      distributionId : 1,
      distributionMethod : "Delivery"
    }
    
    let dummyCouponStatusData2:CouponStatus = {
      couponStatusId : 1,
      couponStatus : "Active"
    }
    
    let dummyMenuItemData2:MenuItem = {
      itemId : 1,
      itemName : "Bacon",
      itemDescription : "Nice Bacon",
      itemCost : 4.50,
      itemImageUrl : "thisurl"
    }
    
    let dummyCouponData2:Coupon = {
      couponCode : "baconspecial",
      menuItem : dummyMenuItemData2,
      couponStatus : dummyCouponStatusData2,
      couponAmount : -1.00
    }
    
    let dummyOrderData2:Order = {
      orderId : 1,
      customer : dummyCustomerData2,
      orderStatus : dummyOrderStatusData2,
      distribution : dummyDistributionData2,
      coupon : dummyCouponData2,
      subtotal : 10,
      tax : 0.50,
      deliveryTip : 0.00,
      total : 10.50,
      orderedTimestamp : null,
      preparingTimestamp : null,
      readyTimestamp : null,
      completedTimestamp : null
    }
  
    let dummyOrderItemData2:OrderItem[] = [{
      orderItemId:1,
      order:dummyOrderData2,
      menuItem:dummyMenuItemData2,
      quantity:1,
      specialInstructions:"does not matter"
    }]

  let orderService:OrderService;
  let orderItemService:OrderitemService;
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;
  let router:Router;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy};
  let httpMock: HttpTestingController;
  let store = {};
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartComponent ]
    })
    .compileComponents();
  });
  
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
          [HttpClientTestingModule]
        ],
        declarations: [ 
          CartComponent,
          CustomerNavBarComponent
        ],
       providers: [
         {provide: OrderService, useClass:MockService},
        {provide: OrderitemService, useClass:MockService},
        {provide: HttpClient, useValue: mockClient}
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;  
    
    spyOn(localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });  
    
    router = TestBed.inject(Router);
    orderService = TestBed.inject(OrderService);
    orderItemService = TestBed.inject(OrderitemService);
    mockClient = TestBed.get(HttpClient);

    spyOn(orderItemService, 'getOrderItemsByOrderId').and.returnValue(Observable.create(observer=>{
      observer.next(dummyOrderItemData2);
    }));

    localStorage.setItem("customerOrder", JSON.stringify(dummyOrderData2));
    localStorage.setItem("customerInfo", JSON.stringify(dummyCustomerData2));
    component.customer = JSON.parse(localStorage.getItem("customerInfo"));
    component.order = JSON.parse(localStorage.getItem("customerOrder"));  
    
    fixture.detectChanges();

  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show cart subtotal', () => {
    let result = fixture.debugElement.query(By.css('#cart-subtotal')).nativeElement;
    expect(result.innerText).toBe('$4.50');
  });

  it('should show cart tax', () => {
    let result = fixture.debugElement.query(By.css('#cart-tax')).nativeElement;
    expect(result.innerText).toBe('$0.23');
  });

  it('should show cart total', () => {
    let result = fixture.debugElement.query(By.css('#cart-total')).nativeElement;
    expect(result.innerText).toBe('$4.73');
  });

  it('should add 1 to the quantity when add is selected', waitForAsync(() => {
    spyOn(orderItemService, 'addOrderItem').and.returnValue(Observable.create(observer=>{
      observer.next('anything');
    }));
    component.order.quantity += 1;
    fixture.whenStable().then(()=>{
      expect(alert).toBe(alert);
    })
  }));

  it('should remove item from cart when remove is selected', waitForAsync(() => {
    spyOn(orderItemService, 'removeOrderItem').and.returnValue(Observable.create(observer=>{
      observer.next('anything');
    }));
    fixture.whenStable().then(()=>{
      expect(alert).toBe(alert);
    })
  }));

  it('should remove all items from cart when clear cart is selected', waitForAsync(() => {
    spyOn(orderItemService, 'removeAllOrderItems').and.returnValue(Observable.create(observer=>{
      observer.next('anything');
    }));
    fixture.whenStable().then(()=>{
      expect(alert).toBe(alert);
    })
  }));

  it('should navgiate to checkout when checkout is selected', waitForAsync(()=> {
    spyOn(orderService, 'updateOrderTotal').and.returnValue(Observable.create(observer=>{
      observer.next("anything");
    }))
    const navigateSpy = spyOn(router, 'navigate');
    component.goToCheckout();
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      expect(navigateSpy).toHaveBeenCalledWith(['/checkout']);
    });
  }));

  it('should navgiate to menu when continue shopping is selected', waitForAsync(()=> {
    const navigateSpy = spyOn(router, 'navigate');
    component.goToMenu();
    fixture.detectChanges();
    fixture.whenStable().then(()=> {
      expect(navigateSpy).toHaveBeenCalledWith(['/menu']);
    });
  }));

});
