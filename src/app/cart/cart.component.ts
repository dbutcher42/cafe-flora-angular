import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '../services/order.service';
import { OrderitemService } from '../services/orderitem.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  // Used until we get session data
  customer;
  orderInfo;
  order;
  orderId;
  orderTax:number;
  orderSubtotal:number;
  orderTotal:number;
  message:string;


  // Inject OrderItemService
  constructor(private router: Router,
    private oiServ: OrderitemService,
    private orServ: OrderService
  ) { }

  ngOnInit(): void {
    this.customer = JSON.parse(localStorage.getItem("customerInfo"));
    this.order = JSON.parse(localStorage.getItem('customerOrder'));

    if (this.customer == null) {
      this.router.navigate(['/login']);
    }

    this.orderId = this.order.orderId;    
    this.getOrderedItems();
  }

  getOrderedItems(){
    this.oiServ.getOrderItemsByOrderId(this.orderId).subscribe(
      response => {
        this.orderInfo = response;
        this.updateTotal();
      }
    )
  }

  goToCheckout(): void {
    this.orServ.updateOrderTotal(this.orderId, this.orderSubtotal, this.orderTax, this.orderTotal).subscribe(
      response => {
        this.order.subtotal = this.orderSubtotal;
        this.order.tax = this.orderTax;
        this.order.total = this.orderTotal;
        localStorage.setItem("customerOrder", JSON.stringify(this.order));
        this.router.navigate(['/checkout']);
        console.log("navigating to checkout");
        this.router.navigate(['/checkout']);
      }
    )
  }

  goToMenu(): void {
    this.router.navigate(['/menu']);
  }

  updateTotal() {
    this.orderSubtotal = 0;
    this.orderTotal = 0;
    this.orderTax = 0;
    
    for(let i = 0; i < this.orderInfo.length; i++){
      this.orderSubtotal += this.orderInfo[i].menuItem.itemCost * this.orderInfo[i].quantity;
    }

    this.orderTax = (this.orderSubtotal * 0.05);
    this.orderTax = +this.orderTax.toPrecision(2);
    this.orderTotal = this.orderSubtotal + this.orderTax;
  }

  addOrderItem(id:number, quantity:number) {
    this.oiServ.addOrderItem(id, quantity).subscribe(
      response => {
        // alert('Item Added');
        this.getOrderedItems();
      }
      );
      this.message = "Quantity Increased";
      setTimeout(() => {this.message = ""}, 1500);
  }

  removeOrderItemId(id:number, index:number) {
    this.orderInfo.splice(index, 1);
    this.oiServ.removeOrderItem(id).subscribe(
      response => {
        // alert('Item Removed');
        this.getOrderedItems();
      }
    );
    this.message = "Item Removed";
    setTimeout(() => {this.message = ""}, 1500);
  }

  clearOrder() {
    this.orderInfo.splice(0, this.orderInfo.length);
    this.orderSubtotal = 0;
    this.orderTotal = 0;
    this.orderTax = 0;
    this.oiServ.removeAllOrderItems(this.order.orderId).subscribe(
      response => {
        // alert('Cart Emptied');
      }
    );
    this.message = "Cart Cleared";
    setTimeout(() => {this.message = ""}, 1500);
  }
}
