import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Customer } from '../services/customer';
import { Order } from '../services/order';
import { OrderService } from '../services/order.service';

@Component({
  selector: 'app-customer-nav-bar',
  templateUrl: './customer-nav-bar.component.html',
  styleUrls: ['./customer-nav-bar.component.css']
})
export class CustomerNavBarComponent implements OnInit, OnDestroy {

  constructor(private router:Router, private ordServ:OrderService) { }

  customer:Customer;
  order:Order;
  orderId:number;
  customerId:number;

  //we get the order/customer data from session data
  ngOnInit(): void {
    this.order = JSON.parse(localStorage.getItem("customerOrder"));
    this.customer = JSON.parse(localStorage.getItem("customerInfo"));
    this.customerId = this.customer.customerId;
    this.orderId = this.order.orderId;
  }

  ngOnDestroy(): void {
  }

  goHome(): void {
    this.router.navigate(['/home']);
  }

  goCart(): void {
    this.router.navigate(['/cart']);
  }

  goMenu(): void {
    console.log("go menu");
    this.router.navigate(['/menu']);
  }

  goHistory(): void {
    this.router.navigate(['/home']);
  }

  goProfile(): void {
    this.router.navigate(['/accountProfile']);
  }

  //before sending them back to the login screen we delete the open/building order
  goLogout(): void {
    this.order = JSON.parse(localStorage.getItem("customerOrder"));
    this.ordServ.deleteOrder(this.orderId).subscribe(
      response => {
        localStorage.clear();
        this.router.navigate(['/login']);
      }
    )
  }

}
