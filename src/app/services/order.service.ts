import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order } from './order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";

  private url1=this.urlbase + "orders/individualorders/";
  private url2=this.urlbase + "orders/orderedorders/";
  private url3=this.urlbase + "orders/completedorders/";
  private url4=this.urlbase + "orders/neworders/";
  private url5=this.urlbase + "orders/";

  httpoptions = {responseType: "text" as "json"}
  
  constructor(private httpClie: HttpClient) { }

  getOrderById(orderId):Observable<Order> {
    return this.httpClie.get<Order>(this.url1 + orderId);
  }

  updateOrder(order):Observable<String> {
    let ordertemp:Order = order;
    return this.httpClie.post<String>(this.url2, ordertemp, this.httpoptions);
  }

  updateOrderTotal(orderId, subtotal, tax, total): Observable<String>{
    orderId = orderId + '';
    subtotal = subtotal + '';
    tax = tax + '';
    total = total + '';
    let map = {"orderId": orderId, "subtotal": subtotal, "tax": tax, "total": total};

    return this.httpClie.put<String>(this.url5 + "addTotal", map, this.httpoptions);
  }

  completeOrder(orderId):Observable<String> {
    return this.httpClie.post<String>(this.url3 + orderId, orderId, this.httpoptions);
  }

  enterNewOrder(customerId):Observable<String> {
    return this.httpClie.post<String>(this.url4 + customerId, customerId, this.httpoptions);
  }

  getNewOrder(customerId):Observable<Order> {
    return this.httpClie.get<Order>(this.url4 + customerId);
  }

  deleteOrder(orderId):Observable<String> {
    return this.httpClie.delete<String>(this.url5 + orderId, this.httpoptions);
    
  }

}
