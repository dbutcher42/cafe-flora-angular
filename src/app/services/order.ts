import { Coupon } from './coupon';
import { Customer } from './customer';
import { Distribution } from './distribution';
import { OrderStatus } from './orderstatus';

export interface Order {
    'orderId':number;
    'customer':Customer;
    'orderStatus':OrderStatus;
    'distribution':Distribution;
    'coupon':Coupon;
    'subtotal':number;
    'tax':number;
    'deliveryTip':number;
    'total':number;
    'orderedTimestamp':number;
    'preparingTimestamp':number;
    'readyTimestamp':number;
    'completedTimestamp':number;
}

