import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MenuItem } from './menuitem'; 

@Injectable({
  providedIn: 'root'
})
export class MenuitemService {

  // private urlbase = "http://ec2-3-17-207-133.us-east-2.compute.amazonaws.com:9025/";
  private urlbase = "//localhost:9025/";

  private url1 = this.urlbase + "menu-items/allmenuitems/";
  private url2 = this.urlbase + "menu-items/individualmenuitems/";

  constructor(private httpClie: HttpClient) { }

  getAllMenuItems():Observable<MenuItem> {
    return this.httpClie.get<MenuItem>(this.url1);
  }

  /*
  getAllMenuItemNames():Observable<MenuItem.itemName> {
    return this.httpClie.get<MenuItem.itemName>(this.url1);
  }
  */

  getMenuItemById(itemId: Number):Observable<MenuItem> {
    return this.httpClie.get<MenuItem>(this.url2 + itemId);
  }
}
