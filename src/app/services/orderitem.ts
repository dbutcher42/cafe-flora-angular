import { MenuItem } from "./menuitem";
import { Order } from "./order";

export interface OrderItem {
    'orderItemId':number;
    'order':Order;
    'menuItem':MenuItem;
    'quantity':number;
    'specialInstructions':String;
}
