export interface MockBank {
    'creditCardNumber':String;
    'creditAmount':number;
    'creditCardName':String;
    'creditCardCode':number;
    'creditCardMonth':number;
    'creditCardYear':number;
}
