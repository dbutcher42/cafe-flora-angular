
export interface MenuItem {
    'itemId':number;
    'itemName':String;
    'itemDescription':String;
    'itemCost':number;
    'itemImageUrl':String;
}
