import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { CustomerHomePageComponent } from './customer-home-page/customer-home-page.component';
import { CustomerNavBarComponent } from './customer-nav-bar/customer-nav-bar.component';
import { RouterModule } from '@angular/router';
import { CustomerCheckoutComponent } from './customer-checkout/customer-checkout.component';
import { CustomerService } from './services/customer.service';
import { OrderService } from './services/order.service';
import { CustomerMenuComponent } from './customer-menu/customer-menu.component';
import { LoginComponent } from './login/login.component';
import { CouponService } from './services/coupon.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DistributionService } from './services/distribution.service';
import { CartComponent } from './cart/cart.component';
import { CustomerPaymentComponent } from './customer-payment/customer-payment.component';
import { MockbankService } from './services/mockbank.service';
import { CustomerOrderVerificationComponent } from './customer-order-verification/customer-order-verification.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './footer/footer.component';
import { CustomerOrderStatusComponent } from './customer-order-status/customer-order-status.component';
import { CustomerProfilePageComponent } from './customer-profile-page/customer-profile-page.component';
import { RegisterComponent } from './register/register.component';
import { ForgetComponent } from './forget/forget.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomerNavBarComponent,
    CustomerCheckoutComponent,
    CartComponent,
    CustomerMenuComponent,
    CustomerHomePageComponent,
    LoginComponent,
    CustomerPaymentComponent,
    CustomerOrderVerificationComponent,
    FooterComponent,
    CustomerOrderStatusComponent,
    CustomerProfilePageComponent,
    RegisterComponent,
    ForgetComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    RouterModule.forRoot([
      {path:'login', component:LoginComponent},
      {path:'register', component:RegisterComponent},
      {path:'forget', component:ForgetComponent},
      {path:'home', component: CustomerHomePageComponent},
      {path:'checkout', component: CustomerCheckoutComponent},
      {path:'cart', component: CartComponent},
      {path: 'menu', component: CustomerMenuComponent},
      {path: 'payment', component: CustomerPaymentComponent},
      {path: 'verified', component: CustomerOrderVerificationComponent},
      {path: 'orderStatus', component: CustomerOrderStatusComponent},
      {path: 'accountProfile', component: CustomerProfilePageComponent},
      {path:'**', redirectTo: 'login'}   //catch all - keep as last path statement
    ]),
  ],
  providers: [CustomerService, OrderService, CouponService, DistributionService, MockbankService],
  bootstrap: [AppComponent]
})
export class AppModule { }
